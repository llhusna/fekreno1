// import something here

import Vuelidate from "vuelidate"
import VueGtag from "vue-gtag";


// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default async ({ Vue })  => {
  Vue.use(Vuelidate)
  Vue.use(VueGtag, {
    config: { id: "G-N94LXM398M" }
  });
}

